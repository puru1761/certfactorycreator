import os
filepath = os.path.abspath(os.path.dirname(__file__))
filepath = os.path.join(filepath, "certs")

def getCertsForAddr(addr):
  with open(os.path.join(filepath, str(addr)+".crt")) as f:
    return [f.read()]

def getPrivateKeyForAddr(addr):
  with open(os.path.join(filepath, str(addr)+".key")) as f:
    return f.read()

def getRootCert(addr):
  with open(os.path.join(filepath, "root.crt")) as f:
    return f.read()
