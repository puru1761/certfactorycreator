#!/bin/bash

address=$1
rootCert=$2

privateKey=private.key

#Password
pass="password"

subj=$(openssl x509 -noout -subject -in $rootCert)
subj=$(echo $subj | sed -e 's/subject= //g')
echo $subj
#Generate new private key for root
openssl genrsa -des3 -passout pass:$pass -out $privateKey 2048 -noout
openssl rsa -in $privateKey -passin pass:$pass -out $privateKey

#Generate new private key for child
openssl genrsa -des3 -passout pass:$pass -out $address.key 2048 -noout
openssl rsa -in $address.key -passin pass:$pass -out $address.key

openssl req -new -key $privateKey -out root.csr -passin pass:$pass	-subj $subj

openssl x509 -req -days 365 -in root.csr -signkey $privateKey -out root.crt

openssl req -new -key $address.key -out $address.csr

openssl x509 -req -days 360 -in $address.csr -CA root.crt -CAkey $privateKey -out $address.crt -set_serial 1

openssl x509 -noout -subject -in $address.crt
openssl x509 -noout -issuer -in $address.crt

mkdir certs
cp $address.key certs/
cp $address.crt certs/
cp root.crt certs/
tar -cvzf CertFactory.tar.gz CertFactory.py certs
rm -rf $address.key $address.csr $address.crt root.crt root.csr private.key
echo ---------SUCCESS----------
